#!/usr/bin/python

import zlib
from trace_event import *
from numpy import *

never_the_same = [ "alfred_topless_", "robotkernel", "rollin_justin.p", "rollin_justin.t" ]
never_the_same_used = dict()

proc_color_offsets = dict()
skip = 0
for i, arg in enumerate(sys.argv[1:]):
    if skip:
        skip -= 1
        continue
    if arg == "-co":
        proc_off = sys.argv[1 + i + 1]
        proc, off = proc_off.rsplit("=", 1)
        off = int(off)
        proc_color_offsets[proc] = off
        skip = 1
        continue
    fn = arg

events = read_ftrace(fn)
print "have %d events" % len(events)

n_cpu = 0
for ev in events:
    if ev.cpu > n_cpu:
        n_cpu = ev.cpu
n_cpu += 1

width = 30


cpu_starts = [0] * n_cpu
cpu_tasks = [None] * n_cpu
cpu_since_last = [0] * n_cpu
cpu_have_new = [False] * n_cpu

colors = "31,32,33,35,36,3730;1,31;1,32;1,33;1,34;1,35;1,36;1,37;1".split(",")

def get_color_hash(tp):
    #cid = id(tp) % len(colors)
    h = abs(zlib.crc32(tp) + proc_color_offsets.get(tp, 0))
    for nts in never_the_same:
        if nts in tp:
            used = never_the_same_used.get(nts)
            #print "\n%r wants %#x" % (nts, h)
            if used is None:
                #print "  new"
                # find free color!
                off = 1
                while True:
                    used = h + off
                    for ntsu in never_the_same_used.values():
                        if ntsu == used:
                            break
                    else:
                        break
                    off += 1
                #print "  found %#x" % used
                never_the_same_used[nts] = used
            h = used
            break
    cid = h % len(colors)
    return "\033[%sm" % colors[cid], "\033[0m"

def output_for(ts, extra="", extra_cpu=None):
    print "%13.6f |" % (ts / 1e6),
    extra_c = "", ""
    for cpu, (task_pid, starts) in enumerate(zip(cpu_tasks, cpu_starts)):
        if task_pid is None:
            tp = ""
            c1, c0 = "", ""
        else:
            tp = "%s/%s" % task_pid
            c1, c0 = get_color_hash(tp)
            t = ts - starts
            if cpu_have_new[ev.cpu]:
                tp = "%3d %s" % (cpu_since_last[cpu], tp)
                cpu_have_new[cpu] = False
            else:
                tp += " %3d" % t
        if extra_cpu == cpu:
            extra_c = c1, c0
        pad = int((width - len(tp)) / 2)
        out = " " * pad
        out += tp
        out += " " * (width - len(tp) - pad)
        print "%s%s%s |" % (c1, out, c0), 
    print "%s%s%s" % (extra_c[0], extra, extra_c[1])

task_last = dict()
have_for_each_cpu = False
for ev in events:
    if ev.event != "sched_switch":
        if have_for_each_cpu:
            output_for(ev.ts, "%d: %s: %s" % (ev.cpu, ev.event, ev.event_args), extra_cpu=ev.cpu)
        continue
    comm = ev.event_args.split("next_comm=", 1)[1]
    comm, pid = comm.split(" next_pid=", 1)
    pid, _ = pid.split(" next", 1)

    this = comm, pid
    task_last_ts = task_last.get(this, ev.ts)
    since_task_last = ev.ts - task_last_ts

    if cpu_tasks[ev.cpu] != this:
        #print "from %r to %r" % (cpu_tasks[ev.cpu], this)
        if not have_for_each_cpu:
            have_for_each_cpu = cpu_starts.count(0) == 0
        else:
            output_for(ev.ts)
        cpu_tasks[ev.cpu] = this
        cpu_starts[ev.cpu] = ev.ts
        cpu_since_last[ev.cpu] = since_task_last
        task_last[this] = ev.ts
        cpu_have_new[ev.cpu] = True

    if have_for_each_cpu:
        output_for(ev.ts)
