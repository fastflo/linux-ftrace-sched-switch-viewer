#!/usr/bin/python

import re
import sys
from numpy import *
import matplotlib.pyplot as plt

if len(sys.argv) > 1:
    trace_fn = sys.argv[1]
else:
    trace_fn = "/sys/kernel/debug/tracing/trace"
    
data = file(trace_fn).read()
print "got %.1fMB of data" % (len(data) / 1024. / 1024., )

only_cpu = 3

expected_cpu = "[%03d]" % only_cpu

t0 = None
block_times = []
start_times = []
max_block_time = None
min_block_time = None
started = None
last_start = None
for line in data.split("\n"):
    if started is None:
        if "user wait for irq->count" in line:
            fields = re.split("[ \t]+", line.strip())
            if fields[1] != expected_cpu:
                continue
            started = float(fields[3][:-1])
            if t0 is None:
                t0 = started
            started -= t0
            start_times.append(started)
            lines_since_start = [line]
        continue
    lines_since_start.append(line)
    if "SARA: got specific interrupt" in line:
        fields = re.split("[ \t]+", line.strip())
        if fields[1] != expected_cpu:
            continue
        woken = float(fields[3][:-1]) - t0
        block_time = woken - started
        if last_start is not None:
            diff_since_last = started - last_start
        else:
            diff_since_last = 0
        if diff_since_last > 0.0003:
            red = "\033[31m"
            unred = " LARGE\033[0m"
        else:
            red = ""
            unred = ""
        print "%s%13.6f: %9.6f: diff since last start: %.6f, block_time: %.6fs%s" % (
            red,
            started + t0, started, diff_since_last, block_time,
            unred
        )
        dump_lines = False
        if max_block_time is None or block_time > max_block_time:
            max_block_time = block_time            
            print "new max_block_time: %.6fs" % max_block_time
            dump_lines = True
        if min_block_time is None or block_time < min_block_time:
            min_block_time = block_time
            print "new min_block_time: %.6fs" % min_block_time
            dump_lines = True
        if dump_lines:
            for line in lines_since_start:
                print "  ", line.rstrip()
            print
            
        block_times.append(block_time)
        last_start = started
        started = None

block_times = array(block_times) * 1e6 # s -> us
start_times = array(start_times)
diff_times = diff(start_times, axis=0) * 1e6

print "diff_times", diff_times.max(), diff_times.min(), diff_times.mean()
print "block_times", block_times.max(), block_times.min(), block_times.mean()

cycle_time = 1 / 8e3 * 1e6
min_cpu_usage = (cycle_time - block_times.max()) / cycle_time
max_cpu_usage = (cycle_time - block_times.min()) / cycle_time
print "cpu usage: min: %.1f%%, max: %.1f%%" % (min_cpu_usage * 100, max_cpu_usage * 100)



if False:
    # the histogram of the data
    n, bins, patches = plt.hist(block_times, 50, alpha=0.75, normed=True)

    plt.xlabel('us')
    #plt.axis([40, 160, 0, 0.03])
    plt.grid(True)
    plt.tight_layout()
    plt.show()
