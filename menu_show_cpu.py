#!/usr/bin/python

import sys
import zlib
from trace_event import *
from numpy import *
from backports.shutil_get_terminal_size import get_terminal_size

never_the_same = [ "serc_irq", "alfred_topless_", "robotkernel", "rollin_justin.p", "rollin_justin.t" ]
never_the_same_used = dict()

proc_color_offsets = dict()
skip = 0
for i, arg in enumerate(sys.argv[1:]):
    if skip:
        skip -= 1
        continue
    if arg == "-co":
        proc_off = sys.argv[1 + i + 1]
        proc, off = proc_off.rsplit("=", 1)
        off = int(off)
        proc_color_offsets[proc] = off
        skip = 1
        continue
    fn = arg

events = read_ftrace(fn)
print "have %d events" % len(events)

n_cpu = 0
for ev in events:
    if ev.cpu > n_cpu:
        n_cpu = ev.cpu
n_cpu += 1

width = 30


cpu_starts = [0] * n_cpu
cpu_tasks = [None] * n_cpu
cpu_since_last = [0] * n_cpu
cpu_have_new = [False] * n_cpu

colors = "31,32,33,35,36,37,30;1,31;1,32;1,33;1,34;1,35;1,36;1,37;1".split(",")
#colors = "31,32".split(",")

def get_color_hash(tp):
    #cid = id(tp) % len(colors)
    h = abs(zlib.crc32(tp) + proc_color_offsets.get(tp, 0))
    for nts in never_the_same:
        if nts in tp:
            used = never_the_same_used.get(nts)
            #print "\n%r wants %#x" % (nts, h)
            if used is None:
                #print "  new"
                # find free color!
                off = 1
                while True:
                    used = h + off
                    for ntsu in never_the_same_used.values():
                        if ntsu == used:
                            break
                    else:
                        break
                    off += 1
                #print "  found %#x" % used
                never_the_same_used[nts] = used
            h = used
            break
    cid = h % len(colors)
    return "\033[%sm" % colors[cid], "\033[0m"



def read_events(events):
    events_out = []
    
    task_last = dict()
    have_for_each_cpu = False
    for ev in events:
        if ev.event != "sched_switch":
            if have_for_each_cpu:
                events_out.append((
                    ev, list(cpu_tasks), list(cpu_starts), dict(task_last), "msg"))
            continue
        comm = ev.event_args.split("next_comm=", 1)[1]
        comm, pid = comm.split(" next_pid=", 1)
        pid, _ = pid.split(" next", 1)

        this = comm, pid
        task_last_ts = task_last.get(this, ev.ts)
        since_task_last = ev.ts - task_last_ts

        if cpu_tasks[ev.cpu] != this:
            #print "from %r to %r" % (cpu_tasks[ev.cpu], this)
            if have_for_each_cpu:
                events_out.append((
                    ev, list(cpu_tasks), list(cpu_starts), dict(task_last), "task_end"))
            cpu_tasks[ev.cpu] = this
            cpu_starts[ev.cpu] = ev.ts
            cpu_since_last[ev.cpu] = since_task_last
            task_last[this] = ev.ts
            cpu_have_new[ev.cpu] = True
            if not have_for_each_cpu:
                have_for_each_cpu = cpu_starts.count(0) == 0
            if have_for_each_cpu:
                # sched_switch event with new task already running?
                events_out.append((
                    ev, list(cpu_tasks), list(cpu_starts), dict(task_last), "new_task", since_task_last))
    return events_out

proc_events = read_events(events)

if False:
    for ev in proc_events:
        print ev
    stop

import time
import termios
import atexit
import traceback
import select
import datetime
import signal
import errno

class terminal(object):
    # https://en.wikipedia.org/wiki/ANSI_escape_code#Escape_sequences
    def __init__(self):
        self.need_restore_buffer = False
        self.saved_attr = termios.tcgetattr(sys.stdout.fileno())
        atexit.register(self.restore)
        
    def restore(self):
        if self.need_restore_buffer:
            self.disable_alternative_buffer()
        termios.tcsetattr(sys.stdout.fileno(), termios.TCSANOW, self.saved_attr)
        
    def get_rows_cols(self):
        cols, rows = get_terminal_size()
        return rows, cols

    def _esc_write(self, what):
        self.write("\033[%s" % what)
        
    def write(self, what):
        sys.stdout.write(what)
        sys.stdout.flush()
        
    def enable_alternative_buffer(self):
        self._esc_write("?1049h")
        self.need_restore_buffer = True
        
    def disable_alternative_buffer(self):
        self._esc_write("?1049l")
        self.need_restore_buffer = False

    def set_cursor(self, row, col):
        self._esc_write("%d;%dH" % (row, col))

    def erase_in_line(self, n=0):
        """
        n = 0: clear from cursor to end of line
        n = 1: clear from cursor to beginning of line
        n = 2: clear entire screen and move to upper left
        n = 3: clear entire screen and delete all liens sved in scrollback buffer
        """
        self._esc_write("%dK" % n)

    def trim_visible_length(self, line, first_visible_col, n_cols):
        output = []
        self.n_hidden_chars = 0
        self.n_visible_chars = 0
        def add_non_escape(what):
            #print "visible: %r" % what
            if self.n_hidden_chars < first_visible_col:
                to_hide = first_visible_col - self.n_hidden_chars
                if to_hide > len(what):
                    to_hide = len(what)
                what = what[to_hide:]
                self.n_hidden_chars += to_hide
            left_to_show = n_cols - self.n_visible_chars
            if len(what) > left_to_show:
                what = what[:left_to_show]
            self.n_visible_chars += len(what)
            output.append(what)
        def add_escape(what):
            #print "non-visible: %r" % what
            output.append(what)
        offset = 0
        while offset < len(line):
            p = line.find("\033[", offset)
            if p == -1:
                add_non_escape(line[offset:])
                break
            if p != offset:
                add_non_escape(line[offset:p])
            e = p + 2

            if line[e:e+6] == "?1049h":
                offset = e + 6
                add_escape(line[p:offset])
            else:
                while e < len(line) and line[e] not in "mH":
                    e += 1
                if e >= len(line):
                    raise Exception("unknown escape sequence at %r in line %r" % (line[p:p+10], line))
                offset = e + 1
                add_escape(line[p:offset])
                
        return "".join(output)
    
    def enable_echo(self, enabled):
        iflag, oflag, cflag, lflag, ispeed, ospeed, cc = termios.tcgetattr(sys.stdout.fileno())
        if enabled:
            lflag |= termios.ECHO
        else:
            lflag &= ~termios.ECHO
        new_attr = [iflag, oflag, cflag, lflag, ispeed, ospeed, cc]
        termios.tcsetattr(sys.stdout.fileno(), termios.TCSANOW, new_attr)
    def enable_canonical(self, enabled):
        iflag, oflag, cflag, lflag, ispeed, ospeed, cc = termios.tcgetattr(sys.stdout.fileno())
        if enabled:
            lflag |= termios.ICANON
        else:
            lflag &= ~termios.ICANON
            cc[termios.VMIN] = 0
            cc[termios.VTIME] = 0
        new_attr = [iflag, oflag, cflag, lflag, ispeed, ospeed, cc]
        termios.tcsetattr(sys.stdout.fileno(), termios.TCSANOW, new_attr)
    
    cursor_down = "\x1b[B"
    cursor_up = "\x1b[A"
    cursor_right = "\x1b[C"
    cursor_left = "\x1b[D"
    cursor_pgup = "\x1b[5~"
    cursor_pgdown = "\x1b[6~"
    cursor_pos1 = "\x1b[H~"
    cursor_ctrl_pos1 = "\x1b[1;5H~"

    F1 = "\x1bOP"
    F2 = "\x1bOQ"
    F3 = "\x1bOR"
    F4 = "\x1bOS"
    F5 = "\x1b[15~"
    F6 = "\x1b[17~"
    F7 = "\x1b[18~"
    F8 = "\x1b[19~"
    F9 = "\x1b[20~"
    F10 = "\x1b[21~"
    F11 = "\x1b[23~"
    F12 = "\x1b[24~"

    Cx_Cx = "\x18\x18"
    Cg_Cg_Cg = "\x07\x07\x07"
    
class event_viewer(object):
    def __init__(self, events):
        self.events = events
        self.term = terminal()

        self.first_event = 0
        self.first_col = 0
        self.cursor_enabled = False
        self.cursor_pos = -1
        self.mark = None
        self.ts_refs = dict()
        self.ts_ref_starts = []

        self.read_string_mode = None
        self.read_string_prompt = ""
        
        self.last_search_string = None

        self.ts_base_offset = events[0][0].ts
        
        self.input_queue = []
        self.debug = True
        
        self.reset_background = ""

        signal.signal(signal.SIGWINCH, self.on_window_size_change)

    def on_window_size_change(self, signo, stack):
        return # just let syscalls receive "interrupted by system call..."
        
    def quit(self):
        self.term.restore()
        sys.exit(0)
        
    def run(self):
        self.term.enable_alternative_buffer()
        self.term.enable_echo(False)
        self.term.enable_canonical(False)
        while True:
            try:
                self.redraw()
                self.read_input()
            except IOError, e:
                if e.errno != errno.EINTR:
                    raise
        self.quit()

    def read_input(self, timeout=None):
        if timeout is None:
            deadline = None
        else:
            deadline = time.time() + timeout
            
        continue_reading = True
        while continue_reading:
            if deadline is not None:
                to_deadline = deadline - time.time()
            else:
                to_deadline = None
            try:
                ready_to_read, _, _ = select.select([sys.stdin], [], [], to_deadline) # todo: listen to WINCH signal!
            except:
                return
            if not ready_to_read:
                break
            continue_reading = True
            while True:
                inp = sys.stdin.read(1)
                if inp == "":
                    break
                if not self.process_input(inp):
                    continue_reading = False
                    break
            self.redraw_menubar()

    def is_outside_screen(self, evid):
        if evid < self.first_event:
            return -1
        if evid >= self.first_event + self.event_rows + 1:
            return 1
        return 0
    def have_cursor_on_screen(self):
        dir = self.is_outside_screen(self.cursor_pos)
        if dir == -1:
            self.cursor_pos = self.first_event
        elif dir == 1:
            self.cursor_pos = self.first_event + self.event_rows - 1

    def have_screen_show_cursor(self):
        if self.is_outside_screen(self.cursor_pos) != 0:
            self.screen_before(self.cursor_pos)
            
    def screen_before(self, idx):
        #screen_start = idx - int(0.25 * self.event_rows)
        screen_start = idx - 10
        if screen_start < 0:
                screen_start = 0
        self.first_event = screen_start

    def event_has_search_pattern(self, idx, pattern):
        return pattern in self.generate_output_line(idx)

    def _search_head(self, pattern):
        if pattern == "":
            if self.last_search_string is None:
                return # nothing todo
            pattern = self.last_search_string
        else:
            self.last_search_string = pattern

        if self.cursor_enabled:
            start_idx = self.cursor_pos
        else:
            start_idx = self.first_event

        return start_idx, pattern
    def _search_bottom(self, start_idx):
        if self.cursor_enabled:
            self.cursor_pos = start_idx
            self.have_screen_show_cursor()
        else:
            self.first_event = start_idx
        
    def search_forward(self, pattern):
        start_idx, pattern = self._search_head(pattern)
        start_idx += 1
        self.ts_offset_idx = -1
        while start_idx < len(self.events):
            if self.event_has_search_pattern(start_idx, pattern):
                break
            start_idx += 1
        else:
            return False # nothing found
        self._search_bottom(start_idx)
            
    def search_backward(self, pattern):
        start_idx, pattern = self._search_head(pattern)
        start_idx -= 1
        while start_idx >= 0:
            self.ts_offset_idx = -1
            if self.event_has_search_pattern(start_idx, pattern):
                break
            start_idx -= 1
        else:
            return False # nothing found
        self._search_bottom(start_idx)
        
    def process_string_command(self):
        seen_input = "".join(self.input_queue)
        self.read_string_mode(seen_input)
        self.read_string_mode = None
        self.read_string_prompt = None
        self.input_queue = []
    
    def start_string_command(self, mode, prompt):
        self.read_string_mode = mode
        self.read_string_prompt = prompt
        self.read_string_input = ""
        self.input_queue = []
        
    def process_input(self, inp):
        if self.read_string_mode:
            if inp == "\n": # finished
                self.process_string_command()
                return False
            else:
                self.input_queue.append(inp)
                self.read_string_input = "".join(self.input_queue)
            return True
        
        if inp == "/": # search forward, enter search pattern input mode
            self.last_search = self.search_forward
            self.start_string_command(self.last_search, "search forward string: ")
            return False
        if inp == "?": # search backward
            self.last_search = self.search_backward
            self.start_string_command(self.last_search, "search backward string: ")
            return False
        if inp == "n" and self.last_search_string is not None:
            self.last_search(self.last_search_string)
            return False
        
        if inp == "\x03" or inp == "q": # sigint
            self.quit()
        if inp == ">": # less jump to end
            self.first_event = len(self.events) - self.event_rows
            if self.cursor_enabled: self.have_cursor_on_screen()
            return False
        if inp == "<": # less jump to start
            self.first_event = 0
            if self.cursor_enabled: self.have_cursor_on_screen()
            return False

        if inp == "\x00": # set mark
            if self.cursor_pos != -1:
                self.mark = self.cursor_pos
            else:
                self.mark = self.first_event
            return False
        
        self.input_queue.append(inp)
        all_in = "".join(self.input_queue)

        if all_in.endswith(terminal.Cg_Cg_Cg):
            self.input_queue = []
            return False

        if self.cursor_pos != -1 and self.mark is not None and all_in.endswith(terminal.Cx_Cx):
            # exchange cursor and mark!
            self.input_queue = []
            self.cursor_pos, self.mark = self.mark, self.cursor_pos

            if self.is_outside_screen(self.cursor_pos):
                self.screen_before(self.cursor_pos)
                if self.first_event < 0:
                    self.first_event = 0
            return False
        
        change_first_event = None
        change_first_col_event = None
        if all_in.endswith(terminal.cursor_down):
            change_first_event = 1
        elif all_in.endswith(terminal.cursor_pgdown):
            change_first_event = self.rows - 1
        elif all_in.endswith(terminal.cursor_up):
            change_first_event = -1
        elif all_in.endswith(terminal.cursor_pgup):
            change_first_event = -(self.rows - 1)
        elif all_in.endswith(terminal.cursor_right):
            change_first_col_event = 1
        elif all_in.endswith(terminal.cursor_left):
            change_first_col_event = -1
            
        if change_first_event is not None:
            self.input_queue = []
            if not self.cursor_enabled:
                # move screen!
                self.first_event += change_first_event
                if self.first_event >= len(self.events):
                    self.first_event = len(self.events) -1
                elif self.first_event < 0:
                    self.first_event = 0
            else:
                # move cursor position
                self.cursor_pos += change_first_event
                if self.cursor_pos >= len(self.events):
                    self.cursor_pos = len(self.events) -1
                elif self.cursor_pos < 0:
                    self.cursor_pos = 0

                if self.cursor_pos >= self.first_event + self.event_rows - 1:
                    self.first_event = self.cursor_pos - self.event_rows + 1
                elif self.cursor_pos < self.first_event:
                    self.first_event = self.cursor_pos
            return False
        elif change_first_col_event is not None:
            self.input_queue = []
            self.first_col += change_first_col_event
            if self.first_col < 0:
                self.first_col = 0
            return False
        
        if all_in.endswith(terminal.F1):
            self.cursor_enabled = not self.cursor_enabled
            if self.cursor_enabled:
                if self.cursor_pos < self.first_event:
                    self.cursor_pos = self.first_event
                elif self.cursor_pos >= self.first_event + self.event_rows:
                    self.cursor_pos = self.first_event + self.event_rows - 1
            self.input_queue = []
            return False
        elif self.cursor_enabled and all_in.endswith(terminal.F2):
            if self.cursor_pos in self.ts_refs:
                del self.ts_refs[self.cursor_pos]
            else:
                ev = self.events[self.cursor_pos][0]
                self.ts_refs[self.cursor_pos] = ev
            self.ts_ref_starts = [ev.ts for ev in self.ts_refs.values()]
            self.ts_ref_starts.sort()
            self.input_queue = []
            return False
        elif self.ts_refs and (all_in.endswith(terminal.F3) or all_in.endswith(terminal.F4)):
            jump_to_next = all_in.endswith(terminal.F4)
            self.input_queue = []
            # first only jump screen,
            # later jump cursor if enabled and set screen accordingly!
            
            # jump screen to prev ts-ref
            now_idx = self.first_event
            if jump_to_next:
                now_idx += self.event_rows - 1
            
            ts_refs = self.ts_refs.keys()
            ts_refs.sort()

            jmp_idx = None
            for idx in ts_refs:
                if idx > now_idx:
                    if jump_to_next:
                        jmp_idx = idx
                    break
                if not jump_to_next:
                    jmp_idx = idx
            if jmp_idx is None:
                # no prev/next ts-ref idx!
                return False
            if self.cursor_enabled:
                self.cursor_pos = jmp_idx
            self.screen_before(jmp_idx)
            
                
            return False

        return True # continue_reading
            
    def redraw_menubar(self):
        self.term.set_cursor(self.rows, 1)

        if self.read_string_prompt:
            self.term.write("%s%s" % (self.read_string_prompt, self.read_string_input))
            self.term.erase_in_line()
            return
        
        if False:
            ts = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            out = [ "%s, " % ts ]
        else:
            out = []
        out.append("first_event: %d (%.1f%%)" % (
            self.first_event, float(self.first_event + self.events_shown) / len(self.events) * 100
        ))
        if self.mark is not None:
            out.append(", mark: %d" % self.mark)
        if self.cursor_pos != -1:
            if self.mark is not None:
                mev = self.events[self.mark][0]
                cev = self.events[self.cursor_pos][0]
                us = cev.ts - mev.ts
                out.append(" -> %+.0fus -> cursor: %d" % (us, self.cursor_pos))
            else:
                out.append(", cursor: %d" % self.cursor_pos)
        out.append(" |")
        if self.cursor_enabled:
            f1_key = "move screen"
        else:
            f1_key = "move cursor"
        out.append(" F1: %s" % f1_key)
        out.append(" F2: toggle ts-ref")
        if self.ts_refs:
            out.append(" F3/F4: jump to next/prev ts-ref")
        if self.cursor_pos != -1:
            out.append(", C-space: set mark at cursor")
        if self.cursor_pos != -1 and self.mark:
            out.append(", C-x C-x: exchange cursor & mark")
        if self.input_queue:
            out.append(", input: %r" % "".join(self.input_queue))
        self.term.write("".join(out))
        self.term.erase_in_line()
        
    def redraw(self):
        self.rows, self.cols = self.term.get_rows_cols()
        self.event_rows = self.rows - 1
        self.term.set_cursor(1, 1)        
        self.events_shown = self.draw_events(n_rows=self.event_rows, n_cols=self.cols - 1)
        self.redraw_menubar()

    def draw_events(self, n_rows, n_cols):
        to_show = min(len(self.events) - self.first_event, n_rows)
        row_idx = self.first_event
        self.ts_offset_idx = -1
        while n_rows > 0:
            n_rows -= 1
            if row_idx < len(self.events):
                with_cursor = self.cursor_pos == row_idx
                with_mark = self.mark == row_idx
            
                if with_cursor:
                    #self.term._esc_write("\033[7m")
                    self.reset_background = "\033[48;5;7m"
                    self.term._esc_write(self.reset_background)
                elif with_mark:
                    self.reset_background = "\033[48;5;6m"
                    self.term._esc_write(self.reset_background)
                line = self.generate_output_line(row_idx)
                line = self.term.trim_visible_length(line, self.first_col, n_cols)                
                self.term.write(line)
                row_idx += 1
            else:
                with_cursor = False
                with_mark = False
                
            self.term.erase_in_line()
            if with_cursor or with_mark:
                self.reset_background = ""
                self.term._esc_write("\033[0m")
            self.term.write("\r\n")
        return to_show
    
    def generate_output_line(self, row_idx):
        row = self.events[row_idx]
        is_ref = row_idx in self.ts_refs
        ev, cpu_tasks, cpu_starts, task_last = row[:4]

        # do we need new ts offset?
        idx = self.ts_offset_idx + 1
        while idx < len(self.ts_ref_starts):
            if self.ts_ref_starts[idx] > ev.ts:
                break # no
            # yes!
            idx += 1
        self.ts_offset_idx = idx - 1
        if self.ts_offset_idx >= 0 and self.ts_offset_idx < len(self.ts_ref_starts):
            ts_offset = self.ts_ref_starts[self.ts_offset_idx]
        else:
            ts_offset = self.ts_base_offset
        
        ev_type = row[4]
        if ev_type == "new_task":
            since_last = row[5]

        if not is_ref:
            #output = [ "%13.6f | " % ((ev.ts - ts_offset - self.ts_base_offset) / 1e6) ]
            output = [ "%13d | " % ((ev.ts - ts_offset)) ]
        else:
            output = [ "%-13.13s | " % "**ref**" ]
        for cpu, (task_pid, starts) in enumerate(zip(cpu_tasks, cpu_starts)):
            if task_pid is None:
                tp = ""
                c1, c0 = "", ""
            else:
                tp = "%s/%s" % task_pid
                c1, c0 = get_color_hash(tp)
                c0 = "\033[0m" + self.reset_background
                t = ev.ts - starts
                if ev_type == "new_task" and ev.cpu == cpu:
                    tp = "%3d %s" % (since_last, tp)
                else:
                    tp += " %3d" % t
            if ev_type == "msg" and ev.cpu == cpu:
                extra_c = c1, c0
            pad = int((width - len(tp)) / 2)
            out = " " * pad
            out += tp
            out += " " * (width - len(tp) - pad)
            output.append("%s%s%s | " % (c1, out, c0))
        
        if ev_type == "msg":
            output.append("%s%d: %s: %s%s" % (extra_c[0], ev.cpu, ev.event, ev.event_args, extra_c[1]))
        
        return "".join(output)
        
        
        
v = event_viewer(proc_events)
try:
    v.run()
except SystemExit:
    pass
except:
    traceback.print_exc()
    v.term.need_restore_buffer = False

sys.exit(0)

skip_first_n_events = find_events_to_skip(events)
print "skip_first_n_events", skip_first_n_events

task_last = dict()
have_for_each_cpu = False
for ev in events:
    if ev.event != "sched_switch":
        if have_for_each_cpu:
            output_for(ev.ts, "%d: %s: %s" % (ev.cpu, ev.event, ev.event_args), extra_cpu=ev.cpu)
        continue
    comm = ev.event_args.split("next_comm=", 1)[1]
    comm, pid = comm.split(" next_pid=", 1)
    pid, _ = pid.split(" next", 1)

    this = comm, pid
    task_last_ts = task_last.get(this, ev.ts)
    since_task_last = ev.ts - task_last_ts

    if cpu_tasks[ev.cpu] != this:
        #print "from %r to %r" % (cpu_tasks[ev.cpu], this)
        if not have_for_each_cpu:
            have_for_each_cpu = cpu_starts.count(0) == 0
        else:
            output_for(ev.ts)
        cpu_tasks[ev.cpu] = this
        cpu_starts[ev.cpu] = ev.ts
        cpu_since_last[ev.cpu] = since_task_last
        task_last[this] = ev.ts
        cpu_have_new[ev.cpu] = True

    if have_for_each_cpu:
        output_for(ev.ts)
