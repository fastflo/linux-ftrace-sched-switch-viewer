#!/usr/bin/python

from trace_event import *
from numpy import *

events = read_ftrace(sys.argv[1])
print "have %d events" % len(events)

marker = "tath_sfun"
expected = 1000 # us

statistics = [
    ("mdl comp time", "have telemetry", "mdlUpdate write done"),
    ("mdl wait time", "wait for telemetry", "have telemetry"),
    ("pltf swap", ("rollin_justin.p", "doing swap write"), "done swapping write"),
    ("tath swap", ("rollin_justin.t", "doing swap write"), "done swapping write"),
]

expected_threshold = expected * 0.2
t0 = events[0].ts
last_ts = 0

kind = {}
any_last = t0

stats = dict()
for name, start, end in statistics:
    stats[name] = dict(starts=[], durations=[])
    
for ev in events:
    for name, start, end in statistics:
        if isinstance(start, tuple):
            comm, start = start
            if ev.task != comm:
                continue
        if start in ev.event_args:
            stats[name]["start"] = ev.ts
        elif end in ev.event_args and "start" in stats[name]:
            stats[name]["starts"].append(stats[name]["start"])
            stats[name]["durations"].append(ev.ts - stats[name]["start"])
            del stats[name]["start"]
    
    if marker not in ev.event_args:
        continue

    key = (ev.event, ev.event_args)
    last_ts = kind.get(key, ev.ts)
    since_last = ev.ts - last_ts # us
    kind[key] = ev.ts

    is_unexpected = abs(since_last - expected) > expected_threshold

    if since_last != 0 and is_unexpected:
        red = "\033[31m"
        unred = "\033[0m"
        add_marker = "  LARGE!"
    else:
        red = ""
        unred = ""
        add_marker = ""

    since_any_last = ev.ts - any_last
    any_last = ev.ts
        
    print "%13.6f: %s%7.0fus%s: %7.0fus: %s: %s %s" % (
        ev.ts / 1e6,
        red, since_last, unred,
        since_any_last,
        ev.event, ev.event_args,
        add_marker
    )


if statistics:
    print "statistics in us:"
    for name, start, end in statistics:
        if not stats[name]["starts"]:
            continue
        starts = array(stats[name]["starts"])
        durations = array(stats[name]["durations"])

        print "  %s: count: %d, min: %3.0f, max: %3.0f, ptp: %3.0f, mean: %3.0f" % (
            name,
            starts.shape[0],
            durations.min(),
            durations.max(),
            durations.ptp(),
            durations.mean()
        )


