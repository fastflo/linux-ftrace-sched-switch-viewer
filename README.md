# ftrace_tools
some tools to work with linux ftrace files

![example](https://rmc-github.robotic.dlr.de/schm-fl/ftrace_tools/blob/master/example.png)

example: log sched events

  cd /sys/kernel/debug/tracing
  # configure tracer:
  echo norecord-cmd > trace_options
  # no function tracer or other modules enabled:
  echo nop > current_tracer
  # enable sched events:
  echo 1 > events/sched/enable
  # empty existing trace:
  echo > trace

  # enable tracing:
  echo 1 > tracing_on

do your stuff, then cp the trace file to disc:

  cp /sys/kernel/debug/tracing/trace /my_traces/blabla

start viewer:

  ./menu_show_cpu.py /my_traces/blabla


you might also want to write your own small processing script to
measure some times of your process. compare check_tath.py for an
example.
