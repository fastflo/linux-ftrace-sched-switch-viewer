import traceback
import cPickle
import os
import sys
import re

class trace_event(object):
    def parse(self, line):
        # read a (non-comment) line from a ascii-ftrace
        hdr, rest = line.rstrip().split(": ", 1)
        hdr = re.split("[ \t]+", hdr.strip())
        o = len(hdr) - 3
        task_pid = " ".join(hdr[:o])
        self.task, pid = task_pid.rsplit("-", 1)
        self.pid = int(pid)
        self.cpu = int(hdr[o][1:-1])

        self.flags = hdr[o + 1] # irqs-off, need-resched, need-resched_lazy, hardirq/softirq, preempt-depth, delay
        self.ts = int(float(hdr[o + 2]) * 1e6) # in us

        self.event, self.event_args = rest.split(": ", 1)

def create_ftrace_pickle(trace_fn):
    events = []
    for line in file(trace_fn).readlines():
        if line[0] == "#":
            continue
        te = trace_event()
        try:
            te.parse(line)
        except:
            print "unparseable: %r" % line
            raise
        events.append(te)

    print "will save %d events" % len(events)
    bn = os.path.basename(trace_fn)
    bn += ".pickle"

    fp = file(bn, "wb")
    cPickle.dump(events, fp, protocol=2)
    return bn

def read_ftrace_pickle(fn):
    fp = file(fn, "rb")
    return cPickle.load(fp)

def read_ftrace(fn, allow_parse=True):
    pickle_fn = fn + ".pickle"
    if os.path.isfile(pickle_fn):
        return read_ftrace_pickle(pickle_fn)
    pickle_fn = os.path.basename(pickle_fn)
    if os.path.isfile(pickle_fn):
        return read_ftrace_pickle(pickle_fn)
    try:
        return read_ftrace_pickle(fn)
    except:
        # try parse!
        pickle_fn = create_ftrace_pickle(fn)
        return read_ftrace_pickle(pickle_fn)
